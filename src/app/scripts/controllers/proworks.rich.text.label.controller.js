angular.module('umbraco').controller('ProWorksRichTextLabelController', function($scope, assetsService, dialogService) {

	//Property Editor has loaded...
	console.log('Hello from ProWorksRichTextLabelController');

	//--------------------------------------------------------------------------------------
	// Event Handlers
	//--------------------------------------------------------------------------------------

	/**
	 * @ngdoc method
	 * @name callbackFromDialog
	 * @function
	 * 
	 * @description
	 * Handles the result from the dialog view/controller
	 */
	$scope.callbackFromDialog = function (data) {

	};

	/**
	 * @ngdoc method
	 * @name openDialog
	 * @function
	 * 
	 * @description
	 * Opens the dialog via the Umbraco dialogService.
	 */
	$scope.openDialog = function () {

		dialogService.open({
			template: '/App_Plugins/ProWorksRichTextLabel/views/proworks.morehelp.dialog.html',
			show: true,
			callback: $scope.callbackFromDialog,
			dialogData: $scope.model.config
		});

	};
});

angular.module('umbraco').controller('ProWorksRichTextLabelDialogController', function($scope, assetsService, dialogService) {


});