angular.module("umbraco").directive('testDirective', function() {

	return {
		restrict: 'E',
		replace: true,
		templateUrl: '/App_Plugins/ProWorksRichTextLabel/views/proworks.test.directive.html'
	};
    
});