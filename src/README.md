# HTML Inline Help Label

This package adds a new property editor that allows you to show HTML in your content pages as properties.  Why is this cool?  Well, we use it to provide inline help to the content editors.  Since it is HTML it can display images or links to more information when the tab or property is complex.  It has also been used to provide a Help tab on a Document Type for specific help for that page type and what they need to know.

You can create several Data Types with this property editor each with their own HTML as prevalues.  

In addition to the inline html as a property, you can specify more HTML to be displayed on the dialog flyout.  This could be used to give in depth info about the page.

After install, create a new Data Type with the type of HTML Inline Help Label.  Then assign it to any Document Type to as a property to display the label on a content page.


## Setup

### Install Dependencies

```bash
npm install -g grunt-cli
npm install
```

### Build

```bash
grunt
```

### Watch

```bash
grunt watch
```

